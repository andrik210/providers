import angular from 'angular';
import { ProvidersModule } from './providers/providers.module';

export const CommonModule = angular
    .module('app.common', [
        ProvidersModule,
    ])
    .name;