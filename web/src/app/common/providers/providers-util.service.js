
export class ProviderUtilService {
    constructor() {
        this.provider = {
            name: {
                min: 4,
                max: 20,
                minMessage: "Provider name must be at least 4 characters long",
                maxMessage: "Provider name must be less than or equal to 20 characters long",
                requiredMessage: 'Provider name is required',
            },
            mcc: {
                length: 3,
                minMessage: "MCC must be 3 characters long",
                requiredMessage: 'MCC is required',
            },
            mnc: {
                min: 2,
                max: 3,
                minMessage: "MNC must be at least 2 characters long",
                maxMessage: "MNC must be less than or equal to 3 characters long",
                requiredMessage: 'MNC is required',
            },
            spn: {
                min: 2,
                max: 15,
                minMessage: "SPN must be at least 2 characters long",
                maxMessage: "SPN must be less than or equal to 15 characters long",
            },
            imsi: {
                min: 12,
                max: 15,
                minMessage: "IMSI must be at least 12 characters long",
                maxMessage: "IMSI must be less than or equal to 15 characters long",
            },
        };
    }
}