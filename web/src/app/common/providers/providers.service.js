export class ProviderService {
    constructor($http) {
        'ngInject';
        this.$http = $http;
        this.url = '/provider';
    }

    getProviders (data) {
        for (var key in data) {
            if(!data[key] )
                data[key] = undefined;
        }

        return this.$http.get(this.url, { params: data }).then((response) => {
            return response.data;
        }, (error) => {
            return [];
        });
    }

    getProvider (id) {
        return this.$http.get(`${this.url}/${id}`).then((response) => {
            return response.data;
        });
    }

    createProvider (model) {
        return this.$http.post(this.url, model).then((response) => {
            return response.data;
        });
    }

    updateProvider (id, model) {
        return this.$http.put(`${this.url}/${id}`, model).then((response) => {
            return response.data;
        });
    }

    deleteProvider (id) {
        return this.$http.delete(`${this.url}/${id}`).then((response) => {
            return response.data;
        });
    }

    detection (data) {
        return this.$http.get('/provider/detect', { params: data }).then((response) => {
            return response.data;
        });
    }

}