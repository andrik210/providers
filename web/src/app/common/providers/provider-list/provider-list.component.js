import template from './provider-list.component.html';

export const ProviderListComponent = {
    template,
    bindings: {
        providers: '<',
        onSelect: '&',
    },
    controller: class ProviderListComponent {
        
        constructor(EventEmitter) {
            this.EventEmitter = EventEmitter;
        }

        $onInit () {

        }

        selectProvider (index) {
            this.onSelect(
                this.EventEmitter({ 
                    provider: this.providers[index],
                    index: index,
                })
            );
        }
    }
};