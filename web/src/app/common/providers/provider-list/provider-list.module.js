import angular from 'angular';
import { ProviderListComponent } from './provider-list.component';

export const ProviderListModule = angular
    .module('provider.list', [])
    .component('providerList', ProviderListComponent)
    .value('EventEmitter', payload => ({ $event: payload }))
    .name;