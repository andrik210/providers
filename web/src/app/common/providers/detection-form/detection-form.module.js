import angular from 'angular';
import { DetectionFromComponent } from './detection-form.component';

export const DetectionFormModule = angular
    .module('detection.form', [])
    .component('detectionForm', DetectionFromComponent)
    .value('EventEmitter', payload => ({ $event: payload }))
    .name;