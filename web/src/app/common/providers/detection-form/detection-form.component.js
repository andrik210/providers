import template from './detection-form.component.html';

export const DetectionFromComponent = {
    template,
    bindings: {
        onChange: '&',
    },
    controller: class DetectionFromComponent {

        constructor (EventEmitter) {
            'ngInject';
            this.detection = {};
            this.EventEmitter = EventEmitter;
        }

        $onInit () {
        }

        submit (form) {
            if (form.$invalid)
                return;

            this.onChange(
                this.EventEmitter({ 
                    search: this.detection
                })
            );
        }
    }
};