import template from './provider-form.component.html';

export const ProviderFormComponent = {
    template,
    bindings: {
        onSave: '&',
        provider: '<?',
        onDelete: '&?',
    },
    controller: class ProviderFormComponent {

        constructor (ProviderService, ProviderUtilService, EventEmitter) {
            'ngInject';
            this.imsiFormat = ProviderService.imsiFormat;
            this.validate = ProviderUtilService.provider;
            this.currentProvider = null;
            this.EventEmitter = EventEmitter;
        }

        $onInit () {
            this.editMode = !!this.provider;

            if (this.editMode) {
                this.currentProvider = angular.copy(this.provider);
            } else {
                this.provider = {
                    spns: [],
                    imsis: [],
                };
            }
        }

        $onDestroy () {
            this.cancel();
        }

        submit (form) {
            if (form.$invalid)
                return;

            if (this.editMode) {
                this.currentProvider = angular.copy(this.provider);
                this.onSave(
                    this.EventEmitter({ provider: this.currentProvider })
                );
            } else {
                this.onSave(
                    this.EventEmitter({ provider: this.provider })
                );
            }
        }

        add (key, form) {
            if (form[key].$invalid || this.provider[key].includes(this[key]))
                return;

            this.provider[key].push(this[key]);
            this[key] = null;
        }

        delete (key, index) {
            this.provider[key].splice(index, 1);
        }

        deleteProvider () {
            this.onDelete(this.EventEmitter({ 
                    provider: this.currentProvider 
                })
            );
        }

        cancel () {
            if (this.editMode) {
                angular.extend(this.provider, this.currentProvider);
            }
        }
    },
};