import angular from 'angular';
import { ProviderFormComponent } from './provider-form.component';

export const ProviderFormModule = angular
    .module('provider.form', [])
    .component('providerForm', ProviderFormComponent)
    .value('EventEmitter', payload => ({ $event: payload }))
    .name;
