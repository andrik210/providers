export const OnEnter = () => {
    return {
        restrict: 'A',
        scope: {
            onEnter: '&'
        },
        link: function (scope, element, attrs) {
            element.on('keypress keydown', function (event) {
                if ((event.which == 13 || event.keyCode == 13) && !(event.shiftKey || event.ctrlKey)) {
                    event.preventDefault();
                    scope.onEnter();
                    scope.$apply();
                }
            });

        }
    }
};

export const Numbers = () => {
    return {
        restrict: 'A',
        link: function(scope, element, attr) {
            element.on('keypress', function (event) {
                event = (event) ? event : window.event;
                var charCode = (event.which) ? event.which : event.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    event.preventDefault();
                }
            });
        }
    }
};