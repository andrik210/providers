import angular from 'angular';
import { FilterFormComponent } from './filter-form.component';

export const FilterFormModule = angular
    .module('filter.form', [])
    .component('filterForm', FilterFormComponent)
    .value('EventEmitter', payload => ({ $event: payload }))
    .name