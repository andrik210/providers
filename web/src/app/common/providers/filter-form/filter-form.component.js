import template from './filter-form.component.html';

export const FilterFormComponent = {
    template,
    bindings: {
        onChange: '&',
    },
    controller: class ProviderFormComponent {

        constructor (ProviderService, EventEmitter) {
            'ngInject';
            this.imsiFormat = ProviderService.imsiFormat;
            this.filter = {};
            this.EventEmitter = EventEmitter;
        }

        $onInit () {

        }

        submit (form) {
            if (form.$invalid)
                return;

            this.onChange(
                this.EventEmitter({ 
                    filter: this.filter,
                })
            );
        }
    }
};