import angular from 'angular';
import uiRouter from 'angular-ui-router';
import { DetectionFormModule } from './detection-form/detection-form.module';
import { FilterFormModule } from './filter-form/filter-form.module';
import { ProviderFormModule } from './provider-form/provider-form.module';
import { ProviderListModule } from './provider-list/provider-list.module';
import { ProvidersComponent } from './providers.component';
import { ProviderService } from './providers.service.js';
import { ProviderUtilService } from './providers-util.service';
import { OnEnter, Numbers } from './providers.directive';
import './providers.css';

export const ProvidersModule = angular
    .module('providers', [
        uiRouter,
        DetectionFormModule,
        FilterFormModule,
        ProviderFormModule,
        ProviderListModule,
    ])
    .directive('onEnter', OnEnter)
    .directive('numbers', Numbers)
    .component('providers', ProvidersComponent)
    .service('ProviderService', ProviderService)
    .service('ProviderUtilService', ProviderUtilService)
    .config(($stateProvider, $urlRouterProvider) => {
        'ngInject';
        $stateProvider
            .state('providers', {
                url: '/',
                component: 'providers',
                resolve: {
                    providers: ProviderService => ProviderService.getProviders(),
                }
            });
        $urlRouterProvider.otherwise('/');
    })
    .name;