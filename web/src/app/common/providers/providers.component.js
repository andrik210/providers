import template from './providers.component.html';

export const ProvidersComponent = {
    template,
    bindings: {
        providers: '<',
    },
    controller: class ProvidersComponent {

        constructor(ProviderService) {
            'ngInject';
            this.ProviderService = ProviderService;
            this.mode = {};
        }

        $onInit() {
        }

        changeFilter({ filter }) {
            this.ProviderService.getProviders(filter).then(data => {
                this.providers = data;
            });
        }

        addProvider({ provider }) {
            this.ProviderService.createProvider(provider).then(data => {
                this.changeMod('new');
                this.providers.unshift(data);
            });
        }

        updateProvider({ provider }) {
            this.ProviderService.updateProvider(provider.id, {
                imsis: provider.imsis,
                mcc: provider.mcc,
                mnc: provider.mnc,
                providerName: provider.providerName,
                spns: provider.spns,
            });
            this.changeMod('edit');
        }

        selectProvider({ provider, index }) {
            this.selectedProvider = provider;
            this.selectedIndex = index;
            this.changeMod('edit');
        }

        deleteProvider({ provider }) {
            this.ProviderService.deleteProvider(provider.id);
            this.providers.splice(this.selectedIndex, 1);
            this.changeMod('edit');
        }

        search({ search }) {
            this.ProviderService.detection(search).then(data => {
                this.providers = [];
                this.providers.push(data);
            }, () => {
                this.providers = [];
            });
        }

        changeMod(key) {
            angular.forEach(this.mode, (e, k) => {
                if (key !== k)
                    this.mode[k] = false;
            });

            this.mode[key] = !this.mode[key];
        }
    }
};