import angular from 'angular';
import uiRouter from 'angular-ui-router';
import ngMessages from 'angular-messages';
import { AppComponent } from './app.component';
import { ComponentsModule } from './components/components.module';
import { CommonModule } from './common/common.module';
import './app.css';

export const AppModule = angular
    .module('app', [
        uiRouter,
        ngMessages,
        ComponentsModule,
        CommonModule,
    ])
    .config(($locationProvider) => {
        'ngInject';
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    })
    .component('app', AppComponent)
    .name;