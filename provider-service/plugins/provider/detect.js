'use strict';

const {isEmpty, isObject} = require('lodash');
const {imsiToMncMcc} = require('../../lib/utils');
const AppError = require('../../lib/app-error');
const Promise = require('bluebird');

const collectionName = 'provider';

const getCollection = function (provider) {
    return new Promise((res, rej) => {
        provider.native$(function (err, db) {
            if (err) return rej(err);
            res(db.collection(collectionName));
        });
    });
};

const nativeFind = function (collection, options) {

    if (!collection) {
        throw new AppError('Collection is not defined.', 400);
    }

    if (!options || !isObject(options)) {
        throw new AppError('Options is invalid.', 400);
    }

    return new Promise((res, rej) => {
        collection.findOne(options, function (err, result) {
            if (err) return rej(err);
            console.log(result);
            res(result);
        });
    });

};

const detectProvider = async function (collection, imsi, spn) {

    let result;
    /*
     * Search in imsi value, if it exists return the provider and finish
     */
    result = await nativeFind(collection, {
        imsis: {
            $elemMatch: {$eq: imsi.toString()}
        }
    });
    if (!isEmpty(result)) return result;

    /*
     * If not, split imsi in mcc and mnc, search by mcc, mnc and regex spn´s. If exists return provider name
     */
    const {mcc, mnc} = imsiToMncMcc(imsi);

    result = await nativeFind(collection, {
        mcc, mnc,
        $where: function () {
            if (!this.spns || Array.isArray(this.spns)) return false;

            for (let spnRegexp of this.spns) {
                const regex = new RegExp(spnRegexp);
                if (regex.test(spn)) return true;
            }
            return false;
        }
    });
    if (!isEmpty(result)) return result;

    /*
     * If not, search by mcc and mnc, if exists return provider and imsi, mcc, mnc and spn
     */
    result = await nativeFind(collection, {mcc, mnc});
    if (!isEmpty(result)) return result;

    return false;
};

/**
 * Returns object from collection "provider" by specific logic :
 *
 * The method must in order of priority:
 * - Search in imsi value, if it exists return the provider and finish
 * - If not, split imsi in mcc and mnc, search by mcc, mnc and regex spn´s. If exists return provider name
 * - If not, search by mcc and mnc, if exists return provider and imsi, mcc, mnc and spn
 * - If not, “404 Not Found” message
 *
 * @param {object} provider
 * @param {String} imsi
 * @param {String} spn
 * @returns {Promise<*|boolean>}
 */
module.exports = async function (provider, {imsi, spn}) {

    if (!imsi) {
        throw new AppError(`Parameter 'imsi' is required.`);
    }

    if (!spn) {
        throw new AppError(`Parameter 'spn' is required.`);
    }

    let collection;
    try {
        collection = await getCollection(provider);
    } catch (err) {
        throw err
    }

    try {
        const result = await detectProvider(collection, imsi, spn);
        if(result) return result;
    } catch (err) {
        throw err
    }

    /*
     * If not, “404 Not Found” message
     */
    throw new AppError('Provider not found.', 404);
};


