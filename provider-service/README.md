# Provider service

Contains source code of microservice which serves managing provider data in mongodb, and exposes port `9001` by default :
```
http://localhost:9001
```  

##### Installation and usage:

Required node.js 8.* or higher.

Install packages :
```
npm install
```

Run app:
```
npm start
```

### Development

Install "nodemon" globally : 
```
npm i nodemon -g
```

Run : 
```
npm run start:development
```

##### Methods: 

*       provider:list 
        
        Example: 
            seneca.act({
                provider: 'list',
                [providerName: (string, optional),]
                [mcc: (string, optional),]
                [mnc: (string, optional),]
                [imsi: (string, optional),]
                [spn: (string, optional),]
            })
            
*       provider:detect
        
        Example: 
            seneca.act({
                provider: 'detect',
                imsi: (string),
                spn: (string)
            }) 

*       provider:read
        
        Example: 
            seneca.act({
                provider: 'read',
                id: (string)               
            }) 

*       provider:create
        
        Example: 
            seneca.act({
                provider: 'create',
                providerName: (string),
                mcc: (string),
                mnc: (string),
                imsis: (string[]),
                spns: (string[]),                                           
            }) 

*       provider:update
        
        Example: 
            seneca.act({
                provider: 'update',
                id: (string),
                providerName: (string, optional),
                mcc: (string, optional),
                mnc: (string, optional),
                imsis: (string[], optional),
                spns: (string[], optional),
            })
            
*       provider:delete

        Example: 
            seneca.act({
                provider: 'read',
                id: (string)               
            }) 

##### Environment variables

* `MONGO_DB_URI` (defaults: 'mongodb://localhost:27017/seneca')
* `PROVIDER_PORT` (defaults: 9002)

##### Dependencies 
* boom
* bluebird
* joi
* lodash
* seneca
* seneca-entity
* seneca-transport
* seneca-basic
* seneca-mongo-store




