'use strict';

const Chairo = require('chairo');
const Hapi = require('hapi');
const routes = require('./config/routes');
const config = require('./config/config');
const {each} = require('lodash');

const server = new Hapi.Server(config.server);

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

(async () => {
    try {
        await server.register({plugin: Chairo});

        // setup seneca
        server.seneca.use('entity');

        // configuring seneca clients
        each(config.senecaClients, senecaClientOptions => server.seneca.client(senecaClientOptions));

        // discovering endpoints
        routes(server);

        await server.start();

        console.info(`Server is listening http://${config.server.host}:${config.server.port}`);
    } catch (err) {
        console.error(err);
    }
})();