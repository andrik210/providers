'use strict';

const {set, clone} = require('lodash');
const joi = require('joi');
const Boom = require('boom');
const validate = require('../lib/validate');

// schema to validate data upon create record
const createSchema = joi.object().keys({
    providerName: joi.string().trim().min(4).max(20).required(),
    mcc: joi.string().trim().length(3).required(),
    mnc: joi.string().trim().min(2).max(3).required(),
    spns: joi.array().items(joi.string().min(2).max(15).trim()),
    imsis: joi.array().items(joi.string().min(12).max(15).trim())
});

// schema to validate data upon update record
const updateSchema = joi.object().keys({
    id: joi.string().trim(),
    providerName: joi.string().trim().min(4).max(20),
    mcc: joi.string().trim().length(3),
    mnc: joi.string().trim().min(2).max(3),
    spns: joi.array().items(joi.string().min(2).max(15).trim()),
    imsis: joi.array().items(joi.string().min(12).max(15).trim())
});

// schema to validate query params
const listQuerySchema = joi.object().keys({
    providerName: joi.string().trim(),
    mcc: joi.string().trim(),
    mnc: joi.string().trim(),
    spn: joi.string().trim(),
    imsi: joi.string().trim()
});

// schema to validate query params
const detectQuerySchema = joi.object().keys({
    spn: joi.string().trim().required(),
    imsi: joi.string().trim().required()
});


module.exports = {
    list: async function (request) {
        const query = clone(request.query) ;
        await validate(query, listQuerySchema);
        return request.seneca.actAsync(set(query, 'provider', 'list'));
    },
    detect: async function (request) {
        const query = clone(request.query) ;
        await validate(query, detectQuerySchema);
        const result = await request.seneca.actAsync(set(query, 'provider', 'detect'));
        if(!result){
            throw Boom.notFound();
        }
        return result;
    },
    read: function (request) {
        return request.seneca.actAsync({
            provider: 'read',
            id: request.params.id
        });
    },
    delete: function (request) {
        return request.seneca.actAsync({
            provider: 'delete',
            id: request.params.id
        });
    },
    create: async function (request) {
        const entity = request.payload;
        await validate(entity, createSchema);
        return request.seneca.actAsync({provider: 'create', entity});
    },
    update: async function (request) {
        const entity = request.payload;
        await validate(entity, updateSchema);
        return request.seneca.actAsync({
            provider: 'update',
            entity: set(entity, 'id', request.params.id)
        });
    }
};
