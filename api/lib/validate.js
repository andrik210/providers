'use strict';

const joi = require('joi');
const Boom = require('boom');

/**
 * Provides validation with joi.
 *
 * @param entity
 * @param schema
 * @returns {Promise<void>}
 */
module.exports = async function (entity, schema) {
    try {
        await joi.validate(entity, schema);
    } catch (err) {
        console.error(err);
        throw Boom.badRequest(err.message);
    }
};