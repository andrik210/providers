'use strict';

const provider = require('../handlers/provider');

module.exports = function (server) {

    server.route({method: 'GET', path: '/ping', handler: req => 'Ok'});

    server.route({method: 'GET', path: '/provider', handler: provider.list});
    server.route({method: 'GET', path: '/provider/detect', handler: provider.detect});
    server.route({method: 'GET', path: '/provider/{id}', handler: provider.read});
    server.route({method: 'DELETE', path: '/provider/{id}', handler: provider.delete});
    server.route({method: 'POST', path: '/provider', handler: provider.create});
    server.route({method: 'PUT', path: '/provider/{id}', handler:  provider.update});
};